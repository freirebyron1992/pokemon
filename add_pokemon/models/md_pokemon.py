# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import fields, models
import requests


class ModelPokemon(models.Model):
    
    _name="pokemon"
    _description="Modulo que importa datos de pokemon"
    
    name = fields.Char("Nombre",size=50)
    url = fields.Char("Url")
    

    def services_request(self):
        OBJ_PKM = self.env["pokemon"]
        data = requests.get(self.url_service()) 
        data = data.json()
        print(data)
        for element in data['results']:
            lst={'name':element['name'],'url':element['url']}
            if OBJ_PKM.search([('name','=',element['name'])]):
                OBJ_PKM.write(lst)
            else:
                OBJ_PKM.create(lst)
    
    def url_service(self):
        
        OBJ_PARAMETER = self.env["ir.config_parameter"].sudo()
        srch_param = OBJ_PARAMETER.search([('key', '=', 'service.pokemon')])
        URL = str(srch_param.value)
        return URL
