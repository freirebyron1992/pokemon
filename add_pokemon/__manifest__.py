# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-
{
    'name': "Pokemos",
   'summary': """ """,

    'description': """ modulo que obtiene datos desde una API externa """,
    'version': '1.0.0.0',
    'category': 'Technical Settings',
    'license': 'LGPL-3',
    'author': "kfc",
    'website': "",
    'contributors': [

    ],
    'data': [
        "data/ir_config_parameter.xml",
        "data/ir_cron.xml",
        "data/res_groups.xml",
        'security/ir.model.access.csv',
        "views/md_pokemon_view.xml",
        "views/ir_ui_menu.xml"    
    ],
    'images': [
    
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
}
