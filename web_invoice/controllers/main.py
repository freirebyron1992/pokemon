# -*- coding: utf-8 -*-
# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import http
from odoo.http import request
import json

class InvoiceData(http.Controller):

    

    @http.route('/invoices/<int:inv_id>', type="http", auth="public", website=True,methods=['GET'])
    def invoice_get_data(self,inv_id):

        def jsonDefault(object):
            return object.__dict__

        invoice_rec = request.env['account.move'].search([('id','=',inv_id)])
        if invoice_rec:
            data = {}
            for r in invoice_rec:
                
                data={
                    "id": r.id,
                    "createtime": str(r.create_date),
                    "document_number": r.name, 
                    "customer":[],
                    "items":[]
                }
                
                data['customer'].append({
                    "document_type": r.partner_id.l10n_latam_identification_type_id.name, 
                    "document_number": r.partner_id.vat, 
                    "first_name": r.partner_id.name,
                    "last_name": r.partner_id.name, 
                    "phone": r.partner_id.phone,
                    "address": str(r.partner_id.street)+' '+str(r.partner_id.street2) ,
                     "email": r.partner_id.email
                    })
                
                for det in r.invoice_line_ids:
                    data['items'].append( {
                    "reference": det.product_id.default_code,
                    "name": det.product_id.product_tmpl_id.name,
                    "price": det.price_unit,
                    "discount": det.discount,
                    "subtotal": det.price_subtotal,
                    "tax": det.price_total-det.price_subtotal ,
                    "total": det.price_total
                    })
                    
        else:
            data = {'mensaje':'Documento no encontrado'}

        return json.dumps(data,default=jsonDefault)



    @http.route('/invoicesc/<string:dicte>', type="http", auth="public", website=True,methods=['GET','POST'])
    def invoice_post_data(self,dicte):
        
        def jsonDefault(object):
            return object.__dict__

        invoice_rec = request.env['account.move']
        invoice_line_rec = request.env['account.move.line']
        partner_rec = request.env['res.partner']
        product_rec = request.env['product.product']
        json_object = json.loads(dicte)
        PARTNER=partner_rec.search([('vat','=',json_object['customer'][0]['document_number'])])
        if len(PARTNER) > 1 or  len(PARTNER) == 0 :
            data = {'mensaje':'Cliente no encontrado'}
        else:
            cdata={
                        "id": json_object['id'],
                        "create_date": json_object['createtime'],
                        "name": json_object['document_number'], 
                        "partner_id":PARTNER.id
                    }
            
            cab_id=invoice_rec.create(cdata)
            for det in json_object['items']:
                PRODUCT=product_rec.search([('default_code','=',det["reference"])],limit=1)
                if len(PRODUCT) == 0 or len(PRODUCT) > 1 :
                    data = {'mensaje':'Producto no encontrado'}
                else:
                    line_move={
                            "product_id": PRODUCT.id,
                            "name": PRODUCT.product_tmpl_id.name,
                            "price_unit": det["price"],
                            "discount": det["discount"],
                            "price_subtotal": det["subtotal"],
                            "price_total": det["total"],
                            "move_id":cab_id.id
                            }
                    invoice_line_rec.create(line_move)


                data = {'mensaje':'Documento creado'}

        return json.dumps(data,default=jsonDefault)


